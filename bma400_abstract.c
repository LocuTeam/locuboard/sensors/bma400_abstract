/*
 * BMA400 abstract driver � 2021 by Miroslav Soukup is licensed under 
 * Attribution-NonCommercial-NoDerivatives 4.0 International. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
*/

/*
  @File Name
    bma400.c

  @Summary
    Abstract driver for Bosch Sensortec bma400 sensor. This abstract driver uses bma400 driver from Bosch Sensortec from web: https://github.com/BoschSensortec/BMA400-API. 
*/


/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */

#include "bma400_abstract.h"
#include "bma400/bma400.h"

#include <stdint.h>



/* ************************************************************************** */
// Section: Interface Functions                                               */
/* ************************************************************************** */


BMA400_ABSTRACT_RSLT_e bma400_abstract_init(bma400_abstract_t *me, uint16_t slave_address, bma400_write_fptr_t i2c_write_funcptr, bma400_read_fptr_t i2c_read_funcptr, bma400_delay_us_fptr_t delay_us_funcptr)
{
    me->_rslt = BMA400_ABSTRACT_OK;
    
    me->slave_address = slave_address;

    me->dev.read  = i2c_read_funcptr;
    me->dev.write = i2c_write_funcptr;
    me->dev.intf  = BMA400_I2C_INTF;

    me->dev.intf_ptr       = &me->slave_address;
    me->dev.delay_us       = delay_us_funcptr;
    me->dev.read_write_len = BMA400_ABSTRACT_READ_WRITE_LENGTH;
    
    me->_dev_rslt = bma400_soft_reset(&me->dev);
    
    me->dev.delay_us(100000, NULL); // Wait for sensor boot up
    
    if(me->_dev_rslt == BMA400_OK){
        me->_dev_rslt = bma400_init(&me->dev);
        
        if(me->_dev_rslt == BMA400_OK){
            me->_rslt = BMA400_ABSTRACT_OK;
        }
        else{
            me->_rslt = BMA400_ABSTRACT_ERR;
        }
    }
    else{
        me->_rslt = BMA400_ABSTRACT_ERR;  
    }
    
    return me->_rslt;
}


void bma400_abstract_convert_raw_data_to_gravity(bma400_abstract_t *me, uint8_t g_range, uint8_t bit_width)
{
    int16_t _accel_ms2;
    int16_t _half_scale;

    _half_scale = 1 << (bit_width - 1);
    
    _accel_ms2 = ((BMA400_ABSTRACT_GRAVITY_EARTH * me->raw_data.x * g_range) * 1000) / _half_scale;
    me->gravity_data.x_int = (int16_t)(_accel_ms2 / 1000);
    me->gravity_data.x_dec = (uint16_t)(BMA400_ABSTRACT_ABS(_accel_ms2 - (me->gravity_data.x_int * 1000)));
    
    _accel_ms2 = ((BMA400_ABSTRACT_GRAVITY_EARTH * me->raw_data.y * g_range) * 1000) / _half_scale;
    me->gravity_data.y_int = (_accel_ms2 / 1000);
    me->gravity_data.y_dec = BMA400_ABSTRACT_ABS(_accel_ms2 - (me->gravity_data.y_int * 1000));
    
    _accel_ms2 = ((BMA400_ABSTRACT_GRAVITY_EARTH * me->raw_data.z * g_range) * 1000) / _half_scale;
    me->gravity_data.z_int = (_accel_ms2 / 1000);
    me->gravity_data.z_dec = BMA400_ABSTRACT_ABS(_accel_ms2 - (me->gravity_data.z_int * 1000));
}


BMA400_ABSTRACT_RSLT_e bma400_abstract_gravity_data_Task(bma400_abstract_t *me)
{
    if(me->abst_sensor_measure_config == BMA400_ABSTRACT_ACCELEROMETER_CONF){

        me->_dev_rslt = bma400_get_interrupt_status(&me->interrupt_status, &me->dev);

        if(me->_dev_rslt == BMA400_OK){

            if (me->interrupt_status & BMA400_ASSERTED_DRDY_INT){
                me->_dev_rslt = bma400_get_accel_data(BMA400_DATA_ONLY, &me->raw_data, &me->dev);
                bma400_abstract_convert_raw_data_to_gravity(me, 2, 12);
                me->_rslt = BMA400_ABSTRACT_DATA_READY;
            }
            else{
                me->_rslt = BMA400_ABSTRACT_DATA_NOT_READY;    
            }
        }
        else{
            me->_rslt = BMA400_ABSTRACT_ERR;
        }
    }
    else{
        me->_rslt = BMA400_ABSTRACT_BAD_CONFIG;
    }
    
    return me->_rslt;
}


BMA400_ABSTRACT_RSLT_e bma400_abstract_accel_act_change_data_Task(bma400_abstract_t *me)
{
    if(me->abst_sensor_measure_config == BMA400_ABSTRACT_ACTIVITY_CHANGE_CONF){
    
        me->_dev_rslt = bma400_get_interrupt_status(&me->interrupt_status, &me->dev);

        if(me->_dev_rslt == BMA400_OK){

            me->_rslt = BMA400_ABSTRACT_DATA_NOT_READY;

            if (me->interrupt_status & BMA400_ASSERTED_ACT_CH_X){
                me->act_change_data.x_count++;

                me->_dev_rslt = bma400_get_accel_data(BMA400_DATA_SENSOR_TIME, &me->raw_data, &me->dev);

                if (me->_dev_rslt == BMA400_OK){
                        me->_rslt = BMA400_ABSTRACT_DATA_READY;
                }
                else{
                    me->_rslt = BMA400_ABSTRACT_ERR;
                }
            }

            if (me->interrupt_status & BMA400_ASSERTED_ACT_CH_Y){

                me->act_change_data.y_count++;

                me->_dev_rslt = bma400_get_accel_data(BMA400_DATA_SENSOR_TIME, &me->raw_data, &me->dev);

                if (me->_dev_rslt == BMA400_OK){
                    me->_rslt = BMA400_ABSTRACT_DATA_READY;
                }
                else{
                    me->_rslt = BMA400_ABSTRACT_ERR;
                }
            }

            if (me->interrupt_status & BMA400_ASSERTED_ACT_CH_Z){

                me->act_change_data.z_count++;

                me->_dev_rslt = bma400_get_accel_data(BMA400_DATA_SENSOR_TIME, &me->raw_data, &me->dev);

                if (me->_dev_rslt == BMA400_OK){
                    me->_rslt = BMA400_ABSTRACT_DATA_READY;
                }
                else{
                    me->_rslt = BMA400_ABSTRACT_ERR;
                }
            }
        }
        else{
            me->_rslt = BMA400_ABSTRACT_ERR;
        }
    }
    else{
        me->_rslt = BMA400_ABSTRACT_BAD_CONFIG;
    }

    return me->_rslt;
}



BMA400_ABSTRACT_RSLT_e bma400_abstract_orientation_Task(bma400_abstract_t *me)
{
    if(me->abst_sensor_measure_config == BMA400_ABSTRACT_ORIENTATION_CONF){

        me->_dev_rslt = bma400_get_sensor_conf(&me->sensor_conf[0], 1, &me->dev);
        
        if (me->_dev_rslt == BMA400_OK){
            
            me->_rslt = BMA400_ABSTRACT_DATA_NOT_READY;
            
            me->_dev_rslt = bma400_get_interrupt_status(&me->interrupt_status, &me->dev);
            
            if (me->_dev_rslt == BMA400_OK){
            
                if (me->interrupt_status & BMA400_ASSERTED_ORIENT_CH){

                    if (me->orient_data.counter == 0){
                        // Setup reference
                        me->orient_data.ref_x = me->sensor_conf[0].param.orient.orient_ref_x;
                        me->orient_data.ref_y = me->sensor_conf[0].param.orient.orient_ref_y;
                        me->orient_data.ref_z = me->sensor_conf[0].param.orient.orient_ref_z;
                    }

                    me->_dev_rslt = bma400_get_accel_data(BMA400_DATA_ONLY, &me->raw_data, &me->dev);

                    if (me->_dev_rslt == BMA400_OK){
                        
                        me->orient_data.orient_direct = BMA400_ABSTRACT_ORIENTATION_UNDEFINED;
                        
                        if(me->raw_data.z > BMA400_ABSTRACT_ORIENTATION_DIRECTIONS_CONSTANT) me->orient_data.orient_direct = BMA400_ABSTRACT_ORIENTATION_UP;
                        else if(me->raw_data.z < (BMA400_ABSTRACT_ORIENTATION_DIRECTIONS_CONSTANT*(-1))) me->orient_data.orient_direct = BMA400_ABSTRACT_ORIENTATION_DOWN;

                        if(me->raw_data.x > BMA400_ABSTRACT_ORIENTATION_DIRECTIONS_CONSTANT) me->orient_data.orient_direct = BMA400_ABSTRACT_ORIENTATION_LEFT;
                        else if(me->raw_data.x < (BMA400_ABSTRACT_ORIENTATION_DIRECTIONS_CONSTANT*(-1))) me->orient_data.orient_direct = BMA400_ABSTRACT_ORIENTATION_RIGHT;
                        
                        if(me->raw_data.y > BMA400_ABSTRACT_ORIENTATION_DIRECTIONS_CONSTANT) me->orient_data.orient_direct = BMA400_ABSTRACT_ORIENTATION_FRONT;
                        else if(me->raw_data.y < (BMA400_ABSTRACT_ORIENTATION_DIRECTIONS_CONSTANT*(-1))) me->orient_data.orient_direct = BMA400_ABSTRACT_ORIENTATION_BACK;
                        
                        me->orient_data.counter++;
                        me->_rslt = BMA400_ABSTRACT_DATA_READY;
                    }
                    else{
                        me->_rslt = BMA400_ABSTRACT_ERR;
                    }
                }
            }
            else{
                me->_rslt = BMA400_ABSTRACT_ERR;
            }
        }
        else{
            me->_rslt = BMA400_ABSTRACT_ERR;
        }
    }
    else{
        me->_rslt = BMA400_ABSTRACT_BAD_CONFIG;
    }
    
    return me->_rslt;
}



BMA400_ABSTRACT_RSLT_e bma400_abstract_step_counter_Task(bma400_abstract_t *me)
{
    if(me->abst_sensor_measure_config == BMA400_ABSTRACT_STEP_COUNTER_CONF){
        
        me->_dev_rslt = bma400_get_interrupt_status(&me->interrupt_status, &me->dev);
        
        if(me->_dev_rslt == BMA400_OK){
            
            me->_rslt = BMA400_ABSTRACT_DATA_NOT_READY;
            
            if (me->interrupt_status & BMA400_ASSERTED_STEP_INT){
                
                me->_dev_rslt = bma400_get_steps_counted(&me->step_counter_data.steps_count, &me->step_counter_data.activity_type, &me->dev);
                
                if(me->_dev_rslt == BMA400_OK){
                    me->_rslt = BMA400_ABSTRACT_DATA_READY;
                }
                else{
                    me->_rslt = BMA400_ABSTRACT_ERR;
                }
            }
        }
        else{
            me->_rslt = BMA400_ABSTRACT_ERR;
        }
    }
    else{
        me->_rslt = BMA400_ABSTRACT_BAD_CONFIG;
    }
    
    return me->_rslt;
}


BMA400_ABSTRACT_RSLT_e bma400_abstract_tap_detection_Task(bma400_abstract_t *me)
{
    if(me->abst_sensor_measure_config == BMA400_ABSTRACT_TAP_DETECTION_CONF){
        
        me->tap_detect_data.single_flag = 0; // Null flag
        me->tap_detect_data.double_flag = 0; // Null flag
        
        me->_dev_rslt = bma400_get_interrupt_status(&me->interrupt_status, &me->dev);

        if (me->_dev_rslt == BMA400_OK){
            
            me->_rslt = BMA400_ABSTRACT_DATA_NOT_READY;
            
            if (me->interrupt_status & BMA400_ASSERTED_S_TAP_INT){
                me->tap_detect_data.single_flag = 1;
                me->tap_detect_data.single_counter++;
                me->_rslt = BMA400_ABSTRACT_DATA_READY;
            }

            if (me->interrupt_status & BMA400_ASSERTED_D_TAP_INT){
                me->tap_detect_data.double_flag = 1;
                me->tap_detect_data.double_counter++;
                me->_rslt = BMA400_ABSTRACT_DATA_READY;
            }
        }
        else{
            me->_rslt = BMA400_ABSTRACT_ERR;
        }
    }
    else{
        me->_rslt = BMA400_ABSTRACT_BAD_CONFIG;
    }
    
    return me->_rslt;
}


uint8_t bma400_abstract_set_configuration(bma400_abstract_t *me, BMA400_ABSTRACT_SENSOR_CONFIGURATION_e configuration, void *additional_config)
{
    uint8_t _state = BMA400_ABSTRACT_ERR;
    
    me->abst_sensor_measure_config = configuration;
    
    switch(me->abst_sensor_measure_config){
    
        case BMA400_ABSTRACT_ACCELEROMETER_CONF:
            /* Select the type of configuration to be modified */
            me->sensor_conf[0].type = BMA400_ACCEL;

            /* Get the accelerometer configurations which are set in the sensor */
            me->_dev_rslt = bma400_get_sensor_conf(&me->sensor_conf[0], 1, &me->dev);
            
            if(me->_dev_rslt == BMA400_OK){
                /* Modify the desired configurations as per macros
                 * available in bma400_defs.h file */
                me->sensor_conf[0].param.accel.odr = BMA400_ODR_100HZ;
                me->sensor_conf[0].param.accel.range = BMA400_RANGE_2G;
                me->sensor_conf[0].param.accel.data_src = BMA400_DATA_SRC_ACCEL_FILT_1;

                /* Set the desired configurations to the sensor */
                me->_dev_rslt = bma400_set_sensor_conf(&me->sensor_conf[0], 1, &me->dev);
               
                if(me->_dev_rslt == BMA400_OK){
                    me->_dev_rslt = bma400_set_power_mode(BMA400_MODE_NORMAL, &me->dev);
                    
                    if(me->_dev_rslt == BMA400_OK){
                        me->interrupt_en[0].type = BMA400_DRDY_INT_EN;
                        me->interrupt_en[0].conf = BMA400_ENABLE;

                        me->_dev_rslt = bma400_enable_interrupt(&me->interrupt_en[0], 1, &me->dev);
                        
                        if(me->_dev_rslt == BMA400_OK){
                            _state = BMA400_ABSTRACT_OK;
                        }
                        else{
                            _state = BMA400_ABSTRACT_ERR;
                        }
                    }
                    else{
                        _state = BMA400_ABSTRACT_ERR;
                    } 
                }
                else{
                    _state = BMA400_ABSTRACT_ERR;
                }
            }
            else{
                _state = BMA400_ABSTRACT_ERR;
            }
        break;
        
        
        case BMA400_ABSTRACT_ACTIVITY_CHANGE_CONF:
            me->sensor_conf[0].type = BMA400_ACTIVITY_CHANGE_INT;
            me->sensor_conf[1].type = BMA400_ACCEL;

            me->_dev_rslt = bma400_get_sensor_conf(me->sensor_conf, 2, &me->dev);
            
            if(me->_dev_rslt == BMA400_OK){
                me->sensor_conf[0].param.act_ch.int_chan = BMA400_INT_CHANNEL_1;
                me->sensor_conf[0].param.act_ch.axes_sel = BMA400_AXIS_XYZ_EN;
                me->sensor_conf[0].param.act_ch.act_ch_ntps = BMA400_ACT_CH_SAMPLE_CNT_64;
                me->sensor_conf[0].param.act_ch.data_source = BMA400_DATA_SRC_ACC_FILT1;
                me->sensor_conf[0].param.act_ch.act_ch_thres = 10;

                me->sensor_conf[1].param.accel.odr = BMA400_ODR_100HZ;
                me->sensor_conf[1].param.accel.range = BMA400_RANGE_2G;
                me->sensor_conf[1].param.accel.data_src = BMA400_DATA_SRC_ACCEL_FILT_1;

                /* Set the desired configurations to the sensor */
                me->_dev_rslt = bma400_set_sensor_conf(&me->sensor_conf[0], 2, &me->dev);
                
                if(me->_dev_rslt == BMA400_OK){
                    me->_dev_rslt = bma400_set_power_mode(BMA400_MODE_NORMAL, &me->dev);
                    
                    if(me->_dev_rslt == BMA400_OK){
                        me->interrupt_en[0].type = BMA400_ACTIVITY_CHANGE_INT_EN;
                        me->interrupt_en[0].conf = BMA400_ENABLE;

                        me->_dev_rslt = bma400_enable_interrupt(&me->interrupt_en[0], 1, &me->dev);
                        
                        if(me->_dev_rslt == BMA400_OK){
                            _state = BMA400_ABSTRACT_OK;
                        }
                        else{
                            _state = BMA400_ABSTRACT_ERR;
                        }
                    }
                    else{
                        _state = BMA400_ABSTRACT_ERR;
                    }
                }
                else{
                    _state = BMA400_ABSTRACT_ERR;
                }
            }
            else{
                _state = BMA400_ABSTRACT_ERR;
            }
        break;
        
        
        case BMA400_ABSTRACT_ORIENTATION_CONF:
            me->sensor_orient_conf = (struct bma400_orient_int_conf *)(additional_config);

            me->sensor_conf[0].type = BMA400_ORIENT_CHANGE_INT;
            me->sensor_conf[1].type = BMA400_ACCEL;

            me->_dev_rslt = bma400_get_sensor_conf(&me->sensor_conf[0], 2, &me->dev);
            
            if(me->_dev_rslt == BMA400_OK){
            
                me->sensor_conf[0].param.orient.axes_sel        = me->sensor_orient_conf->axes_sel;
                me->sensor_conf[0].param.orient.data_src        = me->sensor_orient_conf->data_src;
                me->sensor_conf[0].param.orient.int_chan        = me->sensor_orient_conf->int_chan;
                me->sensor_conf[0].param.orient.orient_int_dur  = me->sensor_orient_conf->orient_int_dur;
                me->sensor_conf[0].param.orient.orient_thres    = me->sensor_orient_conf->orient_thres;
                me->sensor_conf[0].param.orient.ref_update      = me->sensor_orient_conf->ref_update;
                me->sensor_conf[0].param.orient.stability_thres = me->sensor_orient_conf->stability_thres;
                me->sensor_conf[0].param.orient.orient_ref_x    = me->sensor_orient_conf->orient_ref_x;
                me->sensor_conf[0].param.orient.orient_ref_y    = me->sensor_orient_conf->orient_ref_y;
                me->sensor_conf[0].param.orient.orient_ref_z    = me->sensor_orient_conf->orient_ref_z;

                me->sensor_conf[1].param.accel.odr = BMA400_ODR_100HZ;
                me->sensor_conf[1].param.accel.range = BMA400_RANGE_2G;
                me->sensor_conf[1].param.accel.data_src = BMA400_DATA_SRC_ACCEL_FILT_1;

                me->_dev_rslt = bma400_set_sensor_conf(&me->sensor_conf[0], 2, &me->dev);

                if(me->_dev_rslt == BMA400_OK){

                    me->_dev_rslt = bma400_set_power_mode(BMA400_MODE_NORMAL, &me->dev);
                    
                    if(me->_dev_rslt == BMA400_OK){
                    
                        me->interrupt_en[0].type = BMA400_ORIENT_CHANGE_INT_EN;
                        me->interrupt_en[0].conf = BMA400_ENABLE;

                        me->interrupt_en[1].type = BMA400_LATCH_INT_EN;
                        me->interrupt_en[1].conf = BMA400_ENABLE;

                        me->_dev_rslt = bma400_enable_interrupt(&me->interrupt_en[0], 2, &me->dev);
                        
                        if(me->_dev_rslt == BMA400_OK){
                        
                            me->sensor_conf[0].param.orient.axes_sel = 0;
                            me->sensor_conf[0].param.orient.data_src = 0;
                            me->sensor_conf[0].param.orient.int_chan = 0;
                            me->sensor_conf[0].param.orient.orient_int_dur = 0;
                            me->sensor_conf[0].param.orient.orient_thres = 0;
                            me->sensor_conf[0].param.orient.ref_update = 0;
                            me->sensor_conf[0].param.orient.stability_thres = 0;
                            me->sensor_conf[0].param.orient.orient_ref_x = 0;
                            me->sensor_conf[0].param.orient.orient_ref_y = 0;
                            me->sensor_conf[0].param.orient.orient_ref_z = 0;

                            me->_dev_rslt = bma400_get_sensor_conf(&me->sensor_conf[0], 1, &me->dev);
                            
                            if(me->_dev_rslt == BMA400_OK){
                                _state = BMA400_ABSTRACT_OK;
                            }
                            else{
                                _state = BMA400_ABSTRACT_ERR;
                            }
                        }
                        else{
                            _state = BMA400_ABSTRACT_ERR;
                        }
                    }
                    else{
                        _state = BMA400_ABSTRACT_ERR;
                    }
                }
                else{
                    _state = BMA400_ABSTRACT_ERR;    
                }
            }
            else{
                _state = BMA400_ABSTRACT_ERR;
            }
        break;
        
        
        case BMA400_ABSTRACT_STEP_COUNTER_CONF:
            me->sensor_int_pin_conf = (enum bma400_int_chan *)(additional_config);
            
            me->sensor_conf[0].type = BMA400_STEP_COUNTER_INT;
            me->sensor_conf[1].type = BMA400_ACCEL;

            me->_dev_rslt = bma400_get_sensor_conf(&me->sensor_conf[0], 2, &me->dev);
            
            if(me->_dev_rslt == BMA400_OK){
            
                me->sensor_conf[0].param.step_cnt.int_chan = *me->sensor_int_pin_conf;

                me->sensor_conf[1].param.accel.odr = BMA400_ODR_100HZ;
                me->sensor_conf[1].param.accel.range = BMA400_RANGE_2G;
                me->sensor_conf[1].param.accel.data_src = BMA400_DATA_SRC_ACCEL_FILT_1;

                /* Set the desired configurations to the sensor */
                me->_dev_rslt = bma400_set_sensor_conf(&me->sensor_conf[0], 2, &me->dev);
                
                if(me->_dev_rslt == BMA400_OK){
                
                    me->_dev_rslt = bma400_set_power_mode(BMA400_MODE_NORMAL, &me->dev);
                    
                    if(me->_dev_rslt == BMA400_OK){
                    
                        me->interrupt_en[0].type = BMA400_STEP_COUNTER_INT_EN;
                        me->interrupt_en[0].conf = BMA400_ENABLE;

                        me->interrupt_en[1].type = BMA400_LATCH_INT_EN;
                        me->interrupt_en[1].conf = BMA400_ENABLE;

                        me->_dev_rslt = bma400_enable_interrupt(&me->interrupt_en[0], 2, &me->dev);
                        
                        if(me->_dev_rslt == BMA400_OK){
                            _state = BMA400_ABSTRACT_OK;
                        }
                        else{
                            _state = BMA400_ABSTRACT_ERR;
                        }
                    }
                    else{
                        _state = BMA400_ABSTRACT_ERR;
                    }
                }
                else{
                    _state = BMA400_ABSTRACT_ERR;
                }
            }
            else{
                _state = BMA400_ABSTRACT_ERR;
            }
        break;
        
        
        case BMA400_ABSTRACT_TAP_DETECTION_CONF:
            me->sensor_int_pin_conf = (enum bma400_int_chan *)(additional_config);
            
            me->sensor_conf[0].type = BMA400_ACCEL;
            me->sensor_conf[1].type = BMA400_TAP_INT;

            me->_dev_rslt = bma400_get_sensor_conf(&me->sensor_conf[0], 2, &me->dev);
            
            if(me->_dev_rslt == BMA400_OK){
            
                me->sensor_conf[0].param.accel.odr = BMA400_ODR_200HZ;
                me->sensor_conf[0].param.accel.range = BMA400_RANGE_16G;
                me->sensor_conf[0].param.accel.data_src = BMA400_DATA_SRC_ACCEL_FILT_1;
                me->sensor_conf[0].param.accel.filt1_bw = BMA400_ACCEL_FILT1_BW_1;

                me->sensor_conf[1].param.tap.int_chan = *me->sensor_int_pin_conf;
                me->sensor_conf[1].param.tap.axes_sel = BMA400_TAP_X_AXIS_EN | BMA400_TAP_Y_AXIS_EN | BMA400_TAP_Z_AXIS_EN;
                me->sensor_conf[1].param.tap.sensitivity = BMA400_TAP_SENSITIVITY_0;
                me->sensor_conf[1].param.tap.tics_th = BMA400_TICS_TH_6_DATA_SAMPLES;
                me->sensor_conf[1].param.tap.quiet = BMA400_QUIET_60_DATA_SAMPLES;
                me->sensor_conf[1].param.tap.quiet_dt = BMA400_QUIET_DT_4_DATA_SAMPLES;

                me->_dev_rslt = bma400_set_sensor_conf(&me->sensor_conf[0], 2, &me->dev);
                
                if(me->_dev_rslt == BMA400_OK){
                
                    me->_dev_rslt = bma400_set_power_mode(BMA400_MODE_NORMAL, &me->dev);
                    
                    if(me->_dev_rslt == BMA400_OK){
                    
                        me->interrupt_en[0].type = BMA400_SINGLE_TAP_INT_EN;
                        me->interrupt_en[0].conf = BMA400_ENABLE;

                        me->interrupt_en[1].type = BMA400_DOUBLE_TAP_INT_EN;
                        me->interrupt_en[1].conf = BMA400_ENABLE;

                        me->_dev_rslt = bma400_enable_interrupt(&me->interrupt_en[0], 2, &me->dev);
                        
                        if(me->_dev_rslt == BMA400_OK){
                            _state = BMA400_ABSTRACT_OK;
                        }
                        else{
                            _state = BMA400_ABSTRACT_ERR;
                        }
                    }
                    else{
                        _state = BMA400_ABSTRACT_ERR;
                    }
                }
                else{
                    _state = BMA400_ABSTRACT_ERR;
                }
            }
            else{
                _state = BMA400_ABSTRACT_ERR;
            }
        break;
        
        
        default:
            _state = BMA400_ABSTRACT_ERR;
        break;
    }
    
    return _state;
}

        