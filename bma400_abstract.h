/*
 * BMA400 abstract driver � 2021 by Miroslav Soukup is licensed under 
 * Attribution-NonCommercial-NoDerivatives 4.0 International. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
*/

/*
  @File Name
    bma400.h

  @Summary
    Abstract driver for Bosch Sensortec bma400 sensor. This abstract driver uses bma400 driver from Bosch Sensortec from web: https://github.com/BoschSensortec/BMA400-API. 
*/


#ifndef _BMA400_ABSTRACT_H    /* Guard against multiple inclusion */
#define _BMA400_ABSTRACT_H


/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */

#include "bma400/bma400.h"

#include <stdint.h>


/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif

    
    /* ************************************************************************** */
    /* Section: Constants                                                         */
    /* ************************************************************************** */
    #define BMA400_ABSTRACT_READ_WRITE_LENGTH  UINT8_C(46)

    #define BMA400_ABSTRACT_ORIENTATION_DIRECTIONS_CONSTANT 730

    #define BMA400_ABSTRACT_GRAVITY_EARTH     (9.80665f) // Earth's gravity in m/s^2
    #define BMA400_ABSTRACT_SENSOR_TICK_TO_S  (0.0000390625f) // 39.0625us per tick

    #define BMA400_ABSTRACT_I2C_ADDRESS_A0_LOW BMA400_I2C_ADDRESS_SDO_LOW
    #define BMA400_ABSTRACT_I2C_ADDRESS_A0_HIGH BMA400_I2C_ADDRESS_SDO_HIGH

    #define BMA400_ABSTRACT_ABS(A) ((A) < 0) ? (A) * (-1) : (A)


    // *****************************************************************************
    // Section: Data Types
    // *****************************************************************************
    
    typedef struct bma400_abstract_descriptor bma400_abstract_t;
    typedef struct bma400_abstract_gravity_data_descriptor bma400_abstract_gravity_data_t;
    typedef struct bma400_abstract_activity_change_data_descriptor bma400_abstract_activity_change_data_t;
    typedef struct bma400_abstract_orientation_data_descriptor bma400_abstract_orientation_data_t;
    typedef struct bma400_abstract_step_counter_data_descriptor bma400_abstract_step_counter_data_t;
    typedef struct bma400_abstract_tap_detection_data_descriptor bma400_abstract_tap_detection_data_t;
    
    
    typedef enum{
        BMA400_ABSTRACT_ERR            = 0, // bma400 abstract returned state of function ERROR
        BMA400_ABSTRACT_OK             = 1, // bma400 abstract returned state of function OK
        BMA400_ABSTRACT_DATA_NOT_READY = 2, // bma400 abstract returned state of function DATA NOT READY
        BMA400_ABSTRACT_DATA_READY     = 3, // bma400 abstract returned state of function DATA READY
        BMA400_ABSTRACT_BAD_CONFIG     = 4  // bma400 abstract returned state of function BAD MEASURE CONFIGURATION
    }BMA400_ABSTRACT_RSLT_e;
    
    
    typedef enum{
        BMA400_ABSTRACT_ACCELEROMETER_CONF   = 0,
        BMA400_ABSTRACT_ACTIVITY_CHANGE_CONF = 1,
        BMA400_ABSTRACT_ORIENTATION_CONF     = 2,
        BMA400_ABSTRACT_STEP_COUNTER_CONF    = 3,
        BMA400_ABSTRACT_TAP_DETECTION_CONF   = 4
    }BMA400_ABSTRACT_SENSOR_CONFIGURATION_e;
    
    
    typedef enum{
        BMA400_ABSTRACT_ORIENTATION_UP        = 0,
        BMA400_ABSTRACT_ORIENTATION_DOWN      = 1,
        BMA400_ABSTRACT_ORIENTATION_LEFT      = 2,
        BMA400_ABSTRACT_ORIENTATION_RIGHT     = 3,
        BMA400_ABSTRACT_ORIENTATION_FRONT     = 4,
        BMA400_ABSTRACT_ORIENTATION_BACK      = 5,
        BMA400_ABSTRACT_ORIENTATION_UNDEFINED = 6
    }BMA400_ABSTRACT_ORIENTATION_DIRECTIONS_e;
    
    
    typedef enum{
        BMA400_ABSTRACT_STEP_COUNTER_STILL_ACTIVITY = 0,
        BMA400_ABSTRACT_STEP_COUNTER_WALK_ACTIVITY  = 1,
        BMA400_ABSTRACT_STEP_COUNTER_RUN_ACTIVITY   = 2
    }BMA400_ABSTRACT_STEP_COUNTER_e;

    
    struct bma400_abstract_gravity_data_descriptor{
        int16_t x_int;
        uint16_t x_dec;

        int16_t y_int;
        uint16_t y_dec;

        int16_t z_int;
        uint16_t z_dec;
    };
    
    
    struct bma400_abstract_activity_change_data_descriptor{
        uint32_t x_count;
        uint32_t y_count;
        uint32_t z_count;
    };
    
    
    struct bma400_abstract_orientation_data_descriptor{
        BMA400_ABSTRACT_ORIENTATION_DIRECTIONS_e orient_direct;
        
        uint32_t counter;
        
        uint16_t ref_x;
        uint16_t ref_y;
        uint16_t ref_z;
    };
    
    
    struct bma400_abstract_step_counter_data_descriptor{
        uint32_t steps_count;
        BMA400_ABSTRACT_STEP_COUNTER_e activity_type;
    };
    
    
    struct bma400_abstract_tap_detection_data_descriptor{
        uint8_t single_flag;
        uint8_t double_flag;
        
        uint32_t single_counter;
        uint32_t double_counter;
    };
    
    
    

    struct bma400_abstract_descriptor{
        
        uint16_t slave_address;
        
        struct bma400_dev dev;
        int8_t _dev_rslt;
        
        BMA400_ABSTRACT_RSLT_e _rslt;
        
        BMA400_ABSTRACT_SENSOR_CONFIGURATION_e abst_sensor_measure_config; 
        
        struct bma400_sensor_conf sensor_conf[3];
        struct bma400_int_enable interrupt_en[3];
        
        struct bma400_orient_int_conf *sensor_orient_conf;
        
        enum bma400_int_chan *sensor_int_pin_conf;

        uint16_t interrupt_status;

        struct bma400_sensor_data raw_data;
        
        bma400_abstract_gravity_data_t gravity_data;
        bma400_abstract_activity_change_data_t act_change_data;
        bma400_abstract_orientation_data_t orient_data;
        bma400_abstract_step_counter_data_t step_counter_data;
        bma400_abstract_tap_detection_data_t tap_detect_data;
    };
    

    // *****************************************************************************
    // Section: Function Prototypes
    // *****************************************************************************
    
    BMA400_ABSTRACT_RSLT_e bma400_abstract_init(bma400_abstract_t *me, uint16_t slave_address, bma400_write_fptr_t i2c_write_funcptr, bma400_read_fptr_t i2c_read_funcptr, bma400_delay_us_fptr_t delay_us_funcptr);
    
    void bma400_abstract_convert_raw_data_to_gravity(bma400_abstract_t *me, uint8_t g_range, uint8_t bit_width);
    
    BMA400_ABSTRACT_RSLT_e bma400_abstract_gravity_data_Task(bma400_abstract_t *me);
    
    BMA400_ABSTRACT_RSLT_e bma400_abstract_accel_act_change_data_Task(bma400_abstract_t *me);

    BMA400_ABSTRACT_RSLT_e bma400_abstract_orientation_Task(bma400_abstract_t *me);
    
    BMA400_ABSTRACT_RSLT_e bma400_abstract_step_counter_Task(bma400_abstract_t *me);
    
    BMA400_ABSTRACT_RSLT_e bma400_abstract_tap_detection_Task(bma400_abstract_t *me);
    
    uint8_t bma400_abstract_set_configuration(bma400_abstract_t *me, BMA400_ABSTRACT_SENSOR_CONFIGURATION_e configuration, void *additional_config);
    

    /* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif /* _EXAMPLE_FILE_NAME_H */

/* *****************************************************************************
 End of File
 */
